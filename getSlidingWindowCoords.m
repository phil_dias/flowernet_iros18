function [stBlocks] = getSlidingWindowCoords(img,windowW0,windowH0,paddingPerc)

[imgH,imgW,~] = size(img);

% size of sliding window
windowMargin_W = ceil(paddingPerc*windowW0); 
windowMargin_H = ceil(paddingPerc*windowH0);

% adjusted W|H : size of each block without padding 
adjW = windowW0-2*windowMargin_W;
adjH = windowH0-2*windowMargin_H;

% number of blocks per dimension (Rows and Columns)
blocksPerR = ceil(imgH/adjH);
blocksPerC = ceil(imgW/adjW);

% number of blocks
numBlocks = blocksPerR*blocksPerC;

% coordinates of the sliding window in each iteration
% upper left-corner
r0 = 1+adjH*((1:blocksPerR)-1) ;
c0 = 1+adjW*((1:blocksPerC)-1) ;
% bottom right-corner
r1 = adjH*((1:blocksPerR)) ; 
c1 = adjW*((1:blocksPerC)) ; 

% pad image according to size of window
imgPad = padarray(img,[windowMargin_H windowMargin_W],'pre');

r1Pad = r1(end)-imgH + windowMargin_H;
c1Pad = c1(end)-imgW + windowMargin_W;
imgPad = padarray(imgPad,[r1Pad c1Pad],'post');

% add corresponding margin to the coordinates that guide block cropping
% from the padded image
r0Adj = r0; % the padding from the image already acts as the + windowMargin_
c0Adj = c0;
r1Adj = r1 + 2*windowMargin_H; 
c1Adj = c1 + 2*windowMargin_W;

% coordinates of sliding window. Each row corresponds to info for each
% iteration
try
    blockCoord = [repelem(r0Adj',blocksPerC,1) repmat(c0Adj',blocksPerR,1)...
              repelem(r1Adj',blocksPerC,1) repmat(c1Adj',blocksPerR,1)];
catch
   'stop' 
end

% coordinates of each block in the original image (for fusion purposes)
fusionCoord = [repelem(r0',blocksPerC,1) repmat(c0',blocksPerR,1)...
              repelem(r1',blocksPerC,1) repmat(c1',blocksPerR,1)];

% crop out 'post' rows|columns introduced by the last blocks
rows2remove = r1Pad;
cols2remove = c1Pad;   

stBlocks = struct('imgPad',imgPad,'numBlocks',numBlocks,...
                  'blockCoord',blockCoord,'fusionCoord',fusionCoord,...
                  'marginH',windowMargin_H,'marginW',windowMargin_W,...
                  'padRight',rows2remove,'padBottom',cols2remove);        