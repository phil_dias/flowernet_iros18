close all
clear all
clc

%% Set option: direct Caffe or Docker
caffe_or_docker = 1; % use Docker
% caffe_or_docker = 0; % use Caffe directly

%% Paths for each dataset
img_path = './images/';
scores_path = './features/';
output_path = './output/';
NetOutput_path = './CNNpredictions/';
% add path to RGR
addpath('./rgr-public/');

txt_Infile = './config/listIn.txt';
txt_Outfile = './config/listOut.txt';

delete(txt_Infile);
delete(txt_Outfile);

% temporary folder that will contain portraits splits of each image
port_path = './Portraits/';
if exist(port_path)
    rmdir(port_path,'s'); 
end
mkdir(port_path);

mkdir([output_path 'Blend/']);
mkdir([output_path 'Binary/']);

% create link to input folder (used in .sh file)
thisPath = pwd;

shOutput = system(['rm -rf ' thisPath '/input']);
shOutput = system(['ln -s ' img_path ' ' thisPath '/input']);

% shell script requires '\' to handle spaces in name, while MATLAB does not
img_path = strrep(img_path,'\','');

% for trained model, control the size of input images
netCrop = 513; % ResNet

%%
% RGR parameters
SE_ = strel('square',80);
m = .1; 
numSets = 10; % number of seeds sets (samplings)
cellSize = 5; % average spacing between samples

th_ = 0.8; % CNN threshold
thH_ = 1.0; % high confidence foreground (higher than)
thL_ = 0.5; % high confidence background (lower than)

%%
filelist = dir([img_path '*.*']);
filelist = {filelist.name}';
filelist(strcmp(filelist,{'.'})) = [];
filelist(strcmp(filelist,{'..'})) = [];
filelist = sortrows(filelist);
[filelist,~] = sort_nat(filelist);

img_num = length(filelist);

for img_idx = 1:img_num
    
    str = filelist{img_idx};
    img = imread([img_path str]);
    
    img_size = [size(img,1) size(img,2)];  

    % define portrait size according to resolution of images composing
    % our datasets  

    %AppleA: 5184x3456; Others: 2704x1520
    if img_size(1) > 2704
        cropSize = 321; % For AppleA
    else
        cropSize = 155; % For other datasets (lower resolution)
    end
       
    % how many surrouding pixels will be included around each window
    % (i.e. margin percentage)
    padPerc = 0.1;    
    
    % width of sliding window (height will be proportional to image size)
    windowH = min(img_size(1),cropSize);
    windowW = min(img_size(2),cropSize);         
        
    stBlocks = getSlidingWindowCoords(img,windowW,windowH,padPerc);

    % coordinates of each portrait (block) in the original image      
    fc = getfield(stBlocks,'fusionCoord');

    % same coordinates but including margins around each portrait 
    % (guide cropping in padded image)
    bc = getfield(stBlocks,'blockCoord');
    imgPad = stBlocks.imgPad;
    
    sizePad = [size(imgPad,1) size(imgPad,2)];
    
    scoreMapB = zeros(sizePad(1),sizePad(2));
    scoreMapF = zeros(sizePad(1),sizePad(2));
                       
    for itB = 1:stBlocks.numBlocks       

        portrait = imgPad(bc(itB,1):bc(itB,3),...
                          bc(itB,2):bc(itB,4),:);               
        portrait = im2uint8(portrait);                       

        portrait_file = [port_path num2str(itB) '.png'];
        imwrite(portrait,portrait_file,'PNG');

        % write in .txt file containing portraits to be processed                
        fid = fopen(txt_Infile,'a');
        fprintf(fid,[num2str(itB) '.png\n']);
        fclose(fid);
        
        % write in .txt file containing IDs for outputs            
        fid = fopen(txt_Outfile,'a');
        fprintf(fid,[num2str(itB) '\n']);
        fclose(fid);        
        
    end
    
    % invokes CNN to extract features from all portraits
    if (caffe_or_docker == 1)
        shOutput = system('sh docker_getFeats.sh');    
    else
        shOutput = system('sh getFeats.sh');    
    end
    
    for itB = 1:stBlocks.numBlocks 
        portrait = imread([port_path num2str(itB) '.png']);
        
        % portrait size
        port_size0 = [size(portrait,1) size(portrait,2)];
        port_area0 = port_size0(1)*port_size0(2);
        borderArea = sum(port_size0)*2 - 4;

        load([NetOutput_path num2str(itB) '_blob_0.mat']);

        data = imresize(data,[netCrop netCrop],'bilinear');
        flipB = data(:,:,1)';
        flipF = data(:,:,2)';

        % crop the padding introduced by the CNN
        scale_ = port_size0./netCrop;
        [maxScale_,dim] = max(scale_);

        r0 = 1; r1 = port_size0(1); 
        c0 = 1; c1 = port_size0(2);

        flipB = imcrop(flipB,[c0 r0 c1-c0 r1-r0]);
        flipF = imcrop(flipF,[c0 r0 c1-c0 r1-r0]);
        
        % crop out the original portrait padding 
        r0 = stBlocks.marginH; r1 = size(flipB,1)-stBlocks.marginH;
        c0 = stBlocks.marginW; c1 = size(flipB,2)-stBlocks.marginW;

        % [xmin ymin width height]
        flipB = imcrop(flipB,[c0 r0 c1-c0-1 r1-r0-1]);
        flipF = imcrop(flipF,[c0 r0 c1-c0-1 r1-r0-1]);

        % remap info to padded image
        rangeR = fc(itB,1):fc(itB,3);
        rangeC = fc(itB,2):fc(itB,4);
                    
        scoreMapB(rangeR,rangeC) = scoreMapB(rangeR,rangeC) + flipB;
        scoreMapF(rangeR,rangeC) = scoreMapF(rangeR,rangeC) + flipF;
        
    end          

    scoreMapB = imcrop(scoreMapB,[1 1 img_size(2)-1 img_size(1)-1]);
    scoreMapF = imcrop(scoreMapF,[1 1 img_size(2)-1 img_size(1)-1]);

    %% RGR 
    
    % original coarse prediction
    M0 = double(scoreMapF > th_*scoreMapB); 

    % identifying high-/low-confidence regions
    maskLikHigh = (scoreMapF > thH_*scoreMapB); % high confidence detection        ;
    maskLikHigh = bwmorph(maskLikHigh,'skel',10);
    maxLikLow = (thL_*scoreMapB < scoreMapF); % low confidence detection           

    maskObjs = maskLikHigh;      

    maskBkg = ~bwmorph(maxLikLow,'thicken',10);
    farBkg = imerode(maskBkg,SE_);
    nearBkg = bitxor(maskBkg,farBkg);

    % build pre segmentation based on these two information
    preSeg = int32(-1*farBkg + nearBkg + 2*maskObjs); 
    RoI = nearBkg + 2*maskObjs;

    % selected starting centroids using grid and distance
    popIdx = find(RoI > 0);
    area_ = numel(popIdx);
    numSamples = ceil(area_/(cellSize^2));

    sppxScores_ = zeros(img_size(1),img_size(2),numSets);       

    zeroMtx = zeros(img_size(1),img_size(2));

    parfor itSet = 1:numSets

        samplesId = floor(area_.*rand(1,area_))+1;
        centIdx_ = popIdx(samplesId(1:numSamples));

        centroids_ = zeroMtx;
        centroids_(centIdx_) = 1;

        % concatened all inputs to Reg.Growing algorithm
        preCat = cat(3,preSeg,uint8(centroids_));

        % perform region growing
        [sppxMap,n,~] = RGR_mex(img,preCat,numSamples,m);            

        % post-reg.growing sppx-wise majority voting
        idx = label2idx(sppxMap);
        N = numel(idx) - 2;

        % get original CNN votes
        sppxSetScores_ = M0;

        for labelVal = 1:N
            Idx = idx{labelVal};
            sppxSetScores_(Idx) = sum((sppxSetScores_(Idx)) > 0)./length(Idx);

        end                              

        sppxScores_(:,:,itSet) = sppxSetScores_;
    end                                       

    % averaging scores obtained for each set of seeds
    sppxScores = sum(sppxScores_,3)./numSets;

    %%
    cmap_ = ind2rgb(gray2ind(mat2gray(sppxScores),255),jet(255));

    fuse_ = imfuse(img,cmap_,'blend');

    [head_,suffix_] = strsplit(str,'.');
    
    imwrite(fuse_,[output_path, 'Blend/' head_{1} '.png'],'png');    
    
    imgBin = img;
    
    maskBin = sppxScores > 0.5;
     % in blue the detected flowers
    shrinkG = ~bwmorph(maskBin,'shrink',2);
    bordersP = bitand(maskBin,shrinkG);

    [y,x] = find(bordersP == 1); 

    for it_px = 1:length(y)
        imgBin(y(it_px),x(it_px),1) = 0;
        imgBin(y(it_px),x(it_px),2) = 0;
        imgBin(y(it_px),x(it_px),3) = 255;
    end    
    
    imwrite(imgBin,[output_path 'Binary/' head_{1} '.png'],'png');    

    scoreMap = cat(3,sppxScores,scoreMapB,scoreMapF);
    save([output_path head_{1} '.mat'],'scoreMap');
    
    % clear auxiliary folders/files
    rmdir(port_path,'s'); mkdir(port_path);
    rmdir(NetOutput_path,'s'); mkdir(NetOutput_path);
    delete(txt_Infile);
    delete(txt_Outfile);
end