#!/bin/sh

## MODIFY PATH for YOUR SETTINGS
export CAFFE_HOME=/usr/local/Frameworks/cocostuff/models/deeplab/deeplab-v2
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64:/usr/local/cuda-8.0/extras/CUPTI/lib64:$LD_LIBRARY_PATH
export PATH=/usr/local/cuda-8.0/bin:$PATH

ROOT_DIR=`pwd`
CAFFE_DIR=/usr/local/Frameworks/cocostuff/models/deeplab/deeplab-v2
CAFFE_BIN=${CAFFE_DIR}/build/tools/caffe
OUTPUT_DIR=./features

WEIGHTS_FILE=./flowerModel_v1.caffemodel
ARCH_FILE0=./deploy_0.prototxt
ARCH_FILE=./deploy.prototxt

cp $ARCH_FILE0 $ARCH_FILE

# GPU device ID
DEV_ID=0

## Create dirs

CONFIG_DIR=./config
echo ${CONFIG_DIR}

TEST_ITER=`cat ${CONFIG_DIR}/listIn.txt | wc -l`

sed -i "s|ROOT_STR|$ROOT_DIR|g" "${ARCH_FILE}"

FEATURE_DIR=${TEMP}
mkdir -p ${OUTPUT_DIR}/output
CMD="sudo ${CAFFE_BIN} test \
--model=${ARCH_FILE} \
--weights=${WEIGHTS_FILE} \
--gpu=${DEV_ID} \
--iterations=${TEST_ITER}"
echo Running ${CMD} && ${CMD}