#!/bin/sh

## MODIFY PATH for YOUR SETTINGS
DOCKER_NAME='deeplab2-docker'
DOCKER_ROOTDIR='/home/data'
OUTPUT_DIR=./features

WEIGHTS_FILE=flowerModel_v1.caffemodel
ARCH_FILE0=deploy_0.prototxt
ARCH_FILE=deploy_Docker.prototxt

ROOT_DIR=`pwd`

cp $ARCH_FILE0 $ARCH_FILE

# GPU ID
DEV_ID=0

## Create dirs

CONFIG_DIR=./config
echo ${CONFIG_DIR}

TEST_ITER=`cat ${CONFIG_DIR}/listIn.txt | wc -l`

sed -i "s|ROOT_STR|$DOCKER_ROOTDIR|g" "${ARCH_FILE}"

mkdir -p ${OUTPUT_DIR}/output
CMD="sudo docker run \
--mount type=bind,source=${ROOT_DIR},target=${DOCKER_ROOTDIR} \
--gpus all -ti ${DOCKER_NAME} \
caffe test --model=${DOCKER_ROOTDIR}/${ARCH_FILE} \
--weights=${DOCKER_ROOTDIR}/${WEIGHTS_FILE} \
--gpu=${DEV_ID} --iterations=${TEST_ITER}"
echo Running ${CMD} && ${CMD}

